package com.eslam.school.models;

import javax.ws.rs.QueryParam;

public class StudentBeanParams {
    private @QueryParam("startId") int start ;
    private @QueryParam("endId") int end;
    private @QueryParam("major") String major;

    public StudentBeanParams(int start, int end, String major) {
        this.start = start;
        this.end = end;
        this.major = major;
    }

    public StudentBeanParams() {
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }
}
