package com.eslam.school.models;

public class Student {
    private int id;
    private String name;
    private String major;
    private String enrolmentYear;



    public Student(){

    }
    public Student(String name, String major, String enrolmentYear) {
        this.name = name;
        this.major = major;
        this.enrolmentYear = enrolmentYear;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getEnrolmentYear() {
        return enrolmentYear;
    }

    public void setEnrolmentYear(String enrolmentYear) {
        this.enrolmentYear = enrolmentYear;
    }
}
