package com.eslam.school.services;

import com.eslam.school.models.Student;
import com.eslam.school.models.myClass;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ClassService {

    private ArrayList<myClass> myClasses =new ArrayList() ;
    private StudentService studentService;
    private static ClassService instance;

    private ClassService(){
        this.studentService=StudentService.getInstance();
        myClasses.addAll (Arrays.asList(
                new myClass("Computer Programming",this.studentService.getStudentsByMajor("CESS")),
                new myClass("Design",this.studentService.getStudentsByMajor("Architecture")),
                new myClass("Power",this.studentService.getStudentsByMajor("Energy"))
                ));
        for (int i = 0; i< myClasses.size(); i++){
            myClasses.get(i).setId(i);
        }

    }
    public static ClassService getInstance(){
        if (instance == null){
            instance = new ClassService();
        }
        return instance;
    }

    public List<myClass> getAllClasses(){
        return myClasses;
    }

    public myClass getClassById (int id){
        List<myClass> updatedMyClasses = this.myClasses.stream()
                .filter((myClass c)-> c.getId() == id).collect(Collectors.toList());
        if (updatedMyClasses.size() == 0) return  null;
        return updatedMyClasses.get(0);
    }
    public List<Student> getClassStudents (int id){
        List<myClass> updatedMyClasses = this.myClasses.stream().filter((myClass c)-> c.getId() == id).collect(Collectors.toList());
        if (updatedMyClasses.size() == 0) return  null;
        return updatedMyClasses.get(0).getStudents();
    }


    public myClass addClass (myClass c){
        c.setId(myClasses.get(myClasses.size()-1).getId()+1); //make sure id is unique
        this.myClasses.add(c);
        return c;
    }
    public myClass updateClass(myClass myClass){
        List<com.eslam.school.models.myClass> updatedMyClasses =  this.myClasses.stream()
                .filter((c)-> c.getId() == myClass.getId()).collect(Collectors.toList());
        if(updatedMyClasses.isEmpty()) return null;
        else{
            myClasses.set(myClass.getId(),myClass);
            return myClass;
        }
    }
    public myClass removeClassById (int id){
        List<myClass> updatedMyClasses = (this.myClasses.stream()
                .filter((myClass c)-> c.getId() == id).collect(Collectors.toList()));

        if (updatedMyClasses.size() == 0) return  null;
        else{
            return myClasses.remove(id);
        }
    }
}
