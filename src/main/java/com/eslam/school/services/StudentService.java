package com.eslam.school.services;

import com.eslam.school.models.Student;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StudentService {

    private List<Student> students = new ArrayList();
    private static StudentService instance;

    private StudentService() {
        students.addAll(Arrays.asList(
                new Student("Eslam", "CESS", "2015"),
                new Student("Ahmed", "CESS", "2016"),
                new Student("Mohamed", "Architecture", "2017"),
                new Student("Kareem", "Architecture", "2017"),
                new Student("Amgad", "Architecture", "2018"),
                new Student("Nour", "Energy", "2020"),
                new Student("Menna", "Energy", "2020"),
                new Student("Tayseer", "Energy", "2015")
        ));
        for (int i = 0; i < students.size(); i++) {
            students.get(i).setId(i);
        }

    }

    public static StudentService getInstance() {
        if (instance == null) {
            instance = new StudentService();
        }
        return instance;
    }

    public List<Student> getAllStudents() {
        return students;
    }

    public List<Student> getStudentsByIdRange(int start, int end) {
        List<Student> students = this.students.stream().
                filter((Student s) -> (s.getId() >= start && s.getId() <= end))
                .collect(Collectors.toList());
        if (students.size() == 0) return null;
        return students;


    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public Student getStudentById(int id) {

        List<Student> updatedStudent = this.students.stream().
                filter((Student s) -> s.getId() == id).collect(Collectors.toList());
        if (updatedStudent.size() == 0) return null;
        return updatedStudent.get(0);
    }

    public Student addStudent(Student s) {
        s.setId(students.get(students.size() - 1).getId() + 1); //make sure id is unique
        this.students.add(s);
        return s;
    }

    public Student updateStudent(Student student) {
        List<Student> updatedStudent = this.students.stream().filter((s) -> {
            if (s.getId() == student.getId()) return true;
            else return false;
        }).collect(Collectors.toList());
        if (updatedStudent.isEmpty()) return null;
        else {
//            student.setId(updatedStudent.get(0).getId());
            students.set(student.getId(), student);
            return student;
        }
    }

    public Student removeStudentById(int id) {
        List<Student> updatedStudent = (this.students.stream().
                filter((Student s) -> s.getId() == id).collect(Collectors.toList()));

        if (updatedStudent.size() == 0) return null;
        else {
//            students.remove(id);
//            System.out.println("Removed: "+ id);
            return students.remove(id);
        }

    }

    public List<Student> getStudentsByMajor(String major) {
        List<Student> students = this.students.stream().filter((s) -> s.getMajor().equalsIgnoreCase(major)).collect(Collectors.toList());
        if (students.size() == 0) return null;
        return students;
    }
}
