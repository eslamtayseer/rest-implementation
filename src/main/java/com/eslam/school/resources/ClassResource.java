package com.eslam.school.resources;

import com.eslam.school.models.ErrorMessage;
import com.eslam.school.models.Student;
import com.eslam.school.models.myClass;
import com.eslam.school.services.ClassService;

import javax.ws.rs.NotFoundException;
import  javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;

public class ClassResource implements  ClassResourceInterface {
    ClassService classService = ClassService.getInstance();
    ErrorMessage notFoundError = new ErrorMessage(404,"Resource Not Found");
    Response response;

    private void checkNotFound(Object object){
        if(object==null) {
            response = Response.status(Response.Status.NOT_FOUND).entity(notFoundError).build();
            throw new NotFoundException(response);
        }
    }

    @Override
    public Response getClasses() {
        List<myClass>  myClasses= classService.getAllClasses();
        checkNotFound(myClasses);
        return Response.ok(myClasses).build();
    }

    @Override
    public Response getClass(int id) {
        myClass myClass= classService.getClassById(id);
        checkNotFound(myClass);
        return Response.ok(myClass).build();
    }

    @Override
    public Response getClassStudents(int id) {
        List<Student> students= classService.getClassStudents(id);
        checkNotFound(students);
        return Response.ok(students).build();
    }

    @Override
    public Response addClass(myClass c, UriInfo uirInfo) {
        URI uri =uirInfo.getAbsolutePathBuilder().path(Integer.toString(c.getId())).build();
        response= Response.created(uri).entity(classService.addClass(c)).build();
        return response;
    }

    @Override
    public Response deleteClass(int id) {
        myClass myClass =classService.removeClassById(id);
        checkNotFound(myClass);
        response =Response.accepted(myClass).build();
        return response;
    }

    @Override
    public Response updateClass(myClass s, int id) {
        s.setId(id);
        myClass myClass= classService.updateClass(s);
        checkNotFound(myClass);
        response =Response.ok(myClass).build();
        return response;
    }
}
