package com.eslam.school.resources;
import com.eslam.school.models.Student;
import com.eslam.school.models.myClass;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Path("/classes")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface ClassResourceInterface{



        @GET
        @Path("/{id}/students")
        public Response getClassStudents(@PathParam("id") int id);

        @GET
        @Path("/{id}")
        public  Response getClass( @PathParam("id") int id);

        @GET
        public Response getClasses();

        @POST
        public Response addClass(myClass c ,@Context UriInfo uirInfo);

        @DELETE
        @Path("/{id}")
        public Response deleteClass(@PathParam("id") int id);

        @PUT
        @Path("/{id}")
        public Response updateClass(myClass c,@PathParam("id") int id );

}