package com.eslam.school.resources;

import com.eslam.school.models.Student;
import com.eslam.school.models.StudentBeanParams;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Path("/students")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface StudentResourceInterface {


    @GET
    public Response getStudents(@BeanParam StudentBeanParams params);

    @GET
    @Path("/{id}")
    public  Response getStudent( @PathParam("id") int id);

    @POST
    public Response addStudent(Student s,@Context UriInfo uirInfo);

    @DELETE
    @Path("/{id}")
    public Response deleteStudent(@PathParam("id") int id);

    @PUT
    @Path("/{id}")
    public Response updateStudent(Student s,@PathParam("id") int id );

}
