package com.eslam.school.resources;


import com.eslam.school.models.ErrorMessage;
import com.eslam.school.models.Student;
import com.eslam.school.models.StudentBeanParams;
import com.eslam.school.services.StudentService;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;

public class StudentResource implements StudentResourceInterface {

    StudentService studentService = StudentService.getInstance();
    ErrorMessage notFoundError = new ErrorMessage(404, "Resource Not Found");
    Response response;

    @Override
    public Response getStudents(StudentBeanParams params) {
        List<Student> studentsByMajor;
        if (params.getMajor() != null) {
            studentsByMajor = studentService.getStudentsByMajor(params.getMajor());
        } else if (params.getStart() >= 0 && params.getEnd() > params.getStart()) {
            studentsByMajor = studentService.getStudentsByIdRange(params.getStart(), params.getEnd());
        } else studentsByMajor = studentService.getAllStudents();

        checkNotFound(studentsByMajor);
        response = Response.ok(studentsByMajor).build();
        return response;
    }

    private void checkNotFound(Object object) {
        if (object == null) {
            response = Response.status(Response.Status.NOT_FOUND).entity(notFoundError).build();
            throw new NotFoundException(response);
        }
    }

    @Override
    public Response getStudent(int id) {
        Student studentById = studentService.getStudentById(id);
//        if(studentById==null) {
//            checkNotFound();
//        }
        checkNotFound(studentById);
        return Response.ok(studentById).build();
    }

    @Override
    public Response addStudent(Student s, UriInfo uirInfo) {
        URI uri = uirInfo.getAbsolutePathBuilder().path(Integer.toString(s.getId())).build();
        response = Response.created(uri).entity(studentService.addStudent(s)).build();
        return response;
    }

    @Override
    public Response deleteStudent(int id) {

        Student student = studentService.removeStudentById(id);
        checkNotFound(student);
        response = Response.accepted(student).build();
        return response;
    }

    @Override
    public Response updateStudent(Student s, int id) {
        s.setId(id);
        Student student = studentService.updateStudent(s);
        checkNotFound(student);
        response = Response.ok(student).build();
        return response;
    }
    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }
}
