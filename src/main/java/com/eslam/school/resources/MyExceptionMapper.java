package com.eslam.school.resources;

import com.eslam.school.models.ErrorMessage;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class MyExceptionMapper implements ExceptionMapper<NotFoundException> {

    @Override
    public Response toResponse(NotFoundException exception) {
        ErrorMessage message =new ErrorMessage(404,"Resource Not Found");
        return Response.status(Response.Status.NOT_FOUND).entity(message).build();
    }
}