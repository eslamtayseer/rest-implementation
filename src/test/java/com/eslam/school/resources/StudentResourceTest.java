package com.eslam.school.resources;

import com.eslam.school.models.Student;
import com.eslam.school.models.StudentBeanParams;
import com.eslam.school.services.StudentService;
//import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.NativeImageTest;
import org.jboss.resteasy.specimpl.ResteasyUriInfo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;
import org.mockito.internal.matchers.Not;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

//@NativeImageTest
class StudentResourceTest {

    @Spy
    StudentService studentService;
    StudentResource studentResource;
    private List<Student> students;

    @BeforeEach
    void setUp() {
        students = Arrays.asList(
                new Student("Eslam", "CESS", "2015"),
                new Student("Ahmed", "CESS", "2016"),
                new Student("Mohamed", "Architecture", "2017")
        );
//        for(int i=0;i<students.size();i++){
//            students.get(i).setId(i);
//        }
        studentService = spy(StudentService.getInstance());
        studentResource = new StudentResource();
        studentResource.setStudentService(studentService);
    }

    @Test
    void getAllStudents() {
        doReturn(students).when(studentService).getAllStudents();
        Response response =studentResource.getStudents(new StudentBeanParams(0, 0, null));
        List<Student> studentList = response.readEntity(List.class);
        verify(studentService).getAllStudents();
        assertEquals(students.size(), studentList.size());
        assertEquals(200, response.getStatus());
    }

    @Test
    void getStudentById() {
        when(studentService.getStudentById(1)).thenReturn(students.get(1));
        Response response = studentResource.getStudent(1);
        Student s = response.readEntity(Student.class);
        verify(studentService).getStudentById(1);
        assertEquals("Ahmed", s.getName());
        assertEquals(200,response.getStatus());
    }

    @Test
    void getStudentByIdNull() {
        when(studentService.getStudentById(4)).thenReturn(null);
        assertThrows(NotFoundException.class, () -> studentResource.getStudent(10).readEntity(Student.class));
    }

    @Test
    void getStudentsByFilter() {
        doReturn(students.subList(0,2)).when(studentService).getStudentsByMajor("CESS");
        Response response =studentResource.getStudents(new StudentBeanParams(0, 0, "CESS"));
        List<Student> studentList = response.readEntity(List.class);
        verify(studentService).getStudentsByMajor("CESS");
        assertAll(
                ()->assertEquals(2, studentList.size()),
                ()->assertEquals("CESS", studentList.get(0).getMajor()),
                ()->assertEquals("CESS", studentList.get(1).getMajor()),
                ()->assertEquals(200, response.getStatus())
        );

    }
    @Test
    void getStudentsByFilterNull() {
        doReturn(null).when(studentService).getStudentsByMajor("Any");
        assertThrows(NotFoundException.class,()->studentResource.getStudents(new StudentBeanParams(0, 0, "Any")));
        verify(studentService).getStudentsByMajor("Any");
    }

    @Test
    void getStudentsPaginatedTest() {
        doReturn(students).when(studentService).getStudentsByIdRange(0,2);
        Response response =studentResource.getStudents(new StudentBeanParams(0, 2, null));
        List<Student> studentList = response.readEntity(List.class);
        verify(studentService).getStudentsByIdRange(0,2);
        assertEquals(students.size(), studentList.size());
        assertEquals(200, response.getStatus());
    }

    @Test  //Testing on the Actual Database
    void checkExceptionThrownOnGet() {
        assertThrows(NotFoundException.class, () -> studentResource.getStudents(new StudentBeanParams(10, 20, null)), "Not Found Exception Should be thrown");
    }

    @Test
    void checkStudentAddedToDatabase() throws URISyntaxException {
        StudentService studentService = mock(StudentService.class); //trying mocking instead of spying
        studentResource.setStudentService(studentService);
        Student s = new Student("Mahmoud","Major","2018");
        when(studentService.addStudent(s)).thenReturn(s);
        Response response= studentResource.addStudent(s,new ResteasyUriInfo(new URI("Test/uri")));
        verify(studentService).addStudent(s);
        assertEquals(201,response.getStatus());
        assertEquals("Mahmoud",response.readEntity(Student.class).getName());
    }

    @Test
    void checkDeleteCalledTest() {
//        List<Student> students= mock(List.class);  //failed Trial To mock Database List class :(
                                                    // Also tried to use spying on List Class to keep default behaviour and it didn't work
//        studentService.setStudents(students);
//        Student student = students.get(1);
//        when(students.stream().
//                filter((Student s) -> s.getId() == 1).collect(Collectors.toList())).thenReturn( students.subList(0,1));
//        when(students.remove(1)).thenReturn(student);
//        when(students.size()).thenReturn(1);
//        Response response= studentResource.deleteStudent(1);
//        verify(studentService).removeStudentById(1);
//        assertEquals(200,response.getStatus());
//        assertEquals("Mahmoud",response.readEntity(Student.class).getName());


        Student s = students.get(1);
        when(studentService.removeStudentById(1)).thenReturn(s);
        Response response= studentResource.deleteStudent(1);
        verify(studentService).removeStudentById(1);
        assertEquals(202,response.getStatus());
        assertEquals("Ahmed",response.readEntity(Student.class).getName());


    }
    @Test
    void checkDeleteCalledTestNull() {
        when(studentService.removeStudentById(5)).thenReturn(null);
        assertThrows(NotFoundException.class, () -> studentResource.deleteStudent(5), "Not Found Exception Should be thrown");
        verify(studentService).removeStudentById(5);
    }
}